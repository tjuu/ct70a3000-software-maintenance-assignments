import os
import unittest
from fib import gold, fib
class TestFib(unittest.TestCase):
    
    def setUp(self):
        self.n = 10
        self.s = 5
        gold(self.n,self.s)
        
    def test_length(self):
        self.assertEqual(len(fib(self.s,self.n)), self.n)

    def test_default(self):
        self.assertEqual(len(fib(self.s)), 10)
        
    def test_start(self):
        self.assertTrue(fib(self.s,self.n)[0] > self.s)

    def test_file(self):
        with open('series.txt') as f1:
            l1 = f1.read().split(" ")
            
        with open('series_gold.txt') as f2:
            l2 = f2.read().split(" ")       
        self.assertEqual(l1,l2)
    
if __name__ == "__main__":
    unittest.main()
    
