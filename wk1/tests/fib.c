#include <stdio.h>
#include "fib.h"

int fib(int n, int s) {
    
    int i, n1 = 0, n2 = 1, next;
    FILE *fp;
    fp = fopen("series.txt","w");


    printf("Series: ");

    for (i = 1; i <= n; ++i)
    {
        if (n1 >= s){ // check start
            printf("%d, ", n1);
            fprintf(fp,"%d, ", n1);
        }      
        next = n1 + n2;
        n1 = n2;
        n2 = next;
    }
    return n1; // return something to test
}

int sum(int a, int b) {
    return a+b;
}

/* int main()
{
    int n, s;
    printf("Number of ints: ");
    scanf("%d", &n);
    
    printf("Starting num: ");
    scanf("%d", &s);

    fib(n, s);
    return 0;

}*/
