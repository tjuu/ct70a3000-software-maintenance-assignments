from multiprocessing import Process, Lock
import requests 
from bs4 import BeautifulSoup 
from bs4.element import Comment
import operator 
from collections import Counter 
import time
import difflib

def start(url): 
    source_code = requests.get(url).text 
    content = ''
    soup = BeautifulSoup(source_code, 'html.parser') 

    for each_text in soup.find_all(string = True):
        if(tag_visible(each_text) == True):
            content = content + each_text

            content = strip_chars(content) 
            words = content.split()

    clean =  list(clean_wordlist(words))
    log_content = ''
    for i in range(0,len(clean)):
        log_content = log_content + clean[i]+ ' '

    return log_content

def crawl(url, lock):
    print('Working on: ', url)
    data = start(url)
    write_log(data,lock,0)

def tag_visible(element):
    if element.parent.name in ['style', 'script', 'meta', '[document]']:
        return False
    if isinstance(element, Comment):
        return False
    return True

def clean_wordlist(wordlist): 
    with open('dict_FIN.txt', 'r') as f:
        r_list = f.read().splitlines()
    for i in range(0, len(r_list)):
        r_list[i] = r_list[i].split(' ')
        r_list[i] = r_list[i].pop(1)
       
    s = set(r_list)
    return(x for x in wordlist if not x.upper() in r_list)


def strip_chars(data):
    allowed = set('ABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖabcdefghijklmnopqrstuvwxyzåäö.#@ ')
    stripped = ''.join(c for c in data if c in allowed)
    return stripped


def write_log(content, lock, mode = 0):
    if mode != 0:
        with open('put_log.txt', 'w') as log:
             return 0
    else: 
        try:
            try:
                lock.acquire()
                with open('put_log.txt', 'a') as log:
                    log.write(content)
            except Exception:
                print('Cannot write to temp file!')
                raise
        finally:
            lock.release() 


def read_log():

    try:
        with open('put_log.txt', 'r') as log:
            return log.read().upper()

    except Exception:
        print('Cannot read from temp file!')
        raise
    

def write_csv_log(data, mode):

    if mode == 0:
            f_name = str(time.strftime('%H-%d-%m-%Y', time.gmtime())) + '-' +'words'+ '-output.csv'
    else:
        f_name = str(time.strftime('%H-%d-%m-%Y', time.gmtime())) + '-' +'sentences'+ '-output.csv'
    try:
        with open('./outputs/' + f_name,'w') as csvfile: 
            for i in range(0, len(data)):
                csvfile.write(str(data[i][0]) + ';' + str(data[i][1] + '\n'))
    except Exception:
        print('Cannot write log')
        raise   

    finally:
        print('Log ' + f_name + ' written.')


def sort(word_list):
    word, amount, final = [], [], []
    ## for matching the amount and words in the final list
    index = 0
    for i in range(0, len(word_list)):
        matches = []
        matches = difflib.get_close_matches((word_list[i]),word, n=1, cutoff = 0.8)
        
        if word_list[i] in word:
            index = word.index(word_list[i])
            amount[index] = amount[index] + 1

        elif matches != []:
            index = word.index(matches[0])
            amount[index] = amount[index] + 1
        else:
            word.append(word_list[i])
            amount.append(1)

    for i in range(0,len(word)):
        final.append([word[i], str(amount[i])])
    return final

def main():

    lock = Lock()
    write_log('flush',lock, 1) ##flush the log file
    try:
        with open('sources.txt', 'r') as f:
            sources = f.read().splitlines()
        
    except IOError as err:
        print('Source file broken')
        raise
    
    p_set = []
    for i in range(0, len(sources)):
        p_set.append(Process(target = crawl, args = (sources[i], lock)))

        print('Started: ' + sources[i])

    for i in range(0, len(p_set)):
        p_set[i].start()

    for i in range(0, len(p_set)):
        p_set[i].join() 
    print('Data collected, analyzing.')
             
    ##words
    data = sort(read_log().split(' '))
    print('Analysis complete, creating log.') 
    write_csv_log(data, 0)

    ##sentances
    data = sort(read_log().split('.'))
    print('Analysis complete, creating log.') 
    write_csv_log(data, 1)
    print('All done!')


# Driver code 
if __name__ == '__main__': 
    main()